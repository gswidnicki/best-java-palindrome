package project;

public class Echo {

	static {
		// przypisanie do zmiennej aktualnego czasu w milisekundach w momencie startu skryptu
		long start = System.currentTimeMillis();
		// wywołanie funkcji pal_num2 obliczającej największy wynik mnożenia, który jest palindromem,
		// jako argomenty funkcji przekazujemy przedział liczb całkowitych, które mają być mnożone
		System.out.println(pal_num2(1000, 9999));
		// przypisanie do zmiennej aktualnego czasu w milisekundach w momencie zakończenia obliczeń
		long koniec = System.currentTimeMillis();
		// wyświetlenie czasu wykonywania skryptu
		System.out.println("Czas wykonania: "+(koniec-start));
		// sprawdzenie poprawności działania funkcji na przykładzie
		assert pal_num2(91, 99) == 9009;
	}

	public static long pal_num2(int a, int b) {
		long wynik = 0;
		long najwiekszy = 0;
		for (int i = a; i <= b; i++) {
			for (int j = a; j <= b; ++j) {
				wynik = i * j;
				// konwersja wyniku mnożenia na string
				String wyn = Long.toString(wynik);
				boolean flag = true;
				// pętla sprawdza czy zmienna jest palindromem
				for (int g = 0; g < Math.round(wyn.length()/2); g++) {
					if (wyn.charAt(g) != wyn.charAt(wyn.length()-1-g)) {
						flag = false;
						break;
					}
				}
				if (flag) {
					najwiekszy = Math.max(najwiekszy, wynik);
				}
			}
		}
		return najwiekszy;
	}
	public static void main(String[] args) {
	}
}
